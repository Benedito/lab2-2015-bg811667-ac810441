package main;

public class RomanNumerals {

//metodo ausiliario dove teniamo solo i simboli fondamentali
// ovvero I,V,X,L,C,D
	public String simboli(int i){
		String number="";
		switch(i){
		case 1:{number="I";
				break;}
		case 5:{number="V";
				break;
				}
		case 10:{number="X";
				 break;
		 		}
		case 50:{number="L";
				 break;
				}
		case 100:{number="C";
		 		  break;
				 }
		case 500:{number="D";
				  break;
				 }
		case 1000:{number="M";
				  break;
				 }
		default:{break;}
		}
		return number;
	}
//------------------------------------------------------------
// Se mod=4 vanno messi prima i simboli fondamentali in base al modulo
// Se mod=9 vanno messi prima i simboli fondamentali in base al modulo	
// Se mod=5 aggiungi il simbolo
// Se mod=3 aggiunge tre simboli consecutivi
	public String arabicToRoman(int i) throws IllegalArgumentException  {
		// TODO Auto-generated method stub
		String number="";
		if(i<=0) throw new IllegalArgumentException (); 
		else{
			
			int j=1;
			int mod;
			while(i!=0){
				
					mod=i%10;
					i=i/10;
					if(mod<=3){
						 
						for(int k=0; k<mod; k++){
							number=simboli(j)+ number;
						}
					}
					if(mod==4){
						if(j>=1000){
							for(int m=0;m<4;m++){
								number=simboli(j)+number;
								}
						}	
						else 
						number=simboli(j)+simboli(5*j)+number;
					
					}
					if(mod==9){
						number=simboli(j)+simboli(10*j)+number;
					}
					if( mod>=5 && mod<9){
						String supp="";
						supp=simboli(5*j);
						for(int k=5; k<mod; k++){
							supp+=simboli(j);
						}
					number=supp+number;
					}
					j*=10;	
					
			
			}
		}
	return number;
	}
	
	public int romanToArabic(String r){
		int numero=0;
		int c1=0;
		int c2=0;
		for(int i=0; i<r.length(); i++){
			c1= daiValore(r.charAt(i));
			if((i+1)>=r.length())
				c2=0;
			else
				c2= daiValore(r.charAt(i+1));
			if( c1 >= c2 ){
				numero= numero + c1;
			}
			else{
				numero= numero + c2 - c1;
				i++;
			}
		}
		return numero;
	}
	
	
	public int daiValore(char c){
		int n=0;
		switch(c){
			case 'I':
				n=1;
				break;
			case 'V':
				n=5;
				break;
			case 'X':
				n=10;
				break;
			case 'L':
				n=50;
				break;
			case 'C':
				n=100;
				break;
			case 'D':
				n=500;
				break;
			case 'M':
				n=1000;
				break;
				
		}
		return n;
	}

}
