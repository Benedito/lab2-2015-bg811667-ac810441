package test;

import static org.junit.Assert.*;
import main.RomanNumerals;

import org.junit.Before;
import org.junit.Test;

public class RomanNumeralsTest {

	private RomanNumerals romanNumerals = new RomanNumerals();

	@Before
	public void setUp(){
		
	}
	
	@Test
	public void one() {
		assertEquals("1", "I", romanNumerals.arabicToRoman(1));
	}
	
	@Test
	public void oneTwo() {
	    assertEquals("1", "I", romanNumerals.arabicToRoman(1));
	    assertEquals("2", "II", romanNumerals.arabicToRoman(2));
    }
	
	@Test
	public void oneTwoThree() {
		assertEquals("1", "I", romanNumerals.arabicToRoman(1));
		assertEquals("2", "II", romanNumerals.arabicToRoman(2));
		assertEquals("3", "III", romanNumerals.arabicToRoman(3));
	}
	
	@Test
	public void four() {
		assertEquals("4", "IV", romanNumerals.arabicToRoman(4));
	}
	
	@Test
	public void five() {
		assertEquals("5", "V", romanNumerals.arabicToRoman(5));
	}
	
	@Test
	public void six() {
		assertEquals("6", "VI", romanNumerals.arabicToRoman(6));
	}
	
	@Test
	public void seven() {
		assertEquals("7", "VII", romanNumerals.arabicToRoman(7));
	}
	
	@Test
	public void eight() {
		assertEquals("8", "VIII", romanNumerals.arabicToRoman(8));
	}
	
	@Test
	public void nineIsXPrefixedByI() {
		assertEquals("9", "IX", romanNumerals.arabicToRoman(9));
	}
	
	@Test
	public void ten() {
		assertEquals("10", "X", romanNumerals.arabicToRoman(10));
	}
	
	@Test
	public void eleven() {
		assertEquals("11", "XI", romanNumerals.arabicToRoman(11));
	}
	
	@Test
	public void twelve() {
		assertEquals("12", "XII", romanNumerals.arabicToRoman(12));
	}
	
	@Test
	public void thirteen() {
		assertEquals("13", "XIII", romanNumerals.arabicToRoman(13));
	}
	
	@Test
	public void fourteen() {
		assertEquals("14", "XIV", romanNumerals.arabicToRoman(14));
	}
	
	@Test
	public void fifteen() {
		assertEquals("15", "XV", romanNumerals.arabicToRoman(15));
	}
	
	@Test
	public void fourty() {
		assertEquals("40", "XL", romanNumerals.arabicToRoman(40));
	}
	
	@Test
	public void fifty() {
		assertEquals("50", "L", romanNumerals.arabicToRoman(50));
	}
	
	@Test
	public void ninety() {
		assertEquals("90", "XC", romanNumerals.arabicToRoman(90));
	}
	
	@Test
	public void oneHundred() {
		assertEquals("100", "C", romanNumerals.arabicToRoman(100));
	}
	
	@Test
	public void fourHundred() {
		assertEquals("400", "CD", romanNumerals.arabicToRoman(400));
	}
	
	@Test
	public void fiveHundred() {
		assertEquals("500", "D", romanNumerals.arabicToRoman(500));
	}
	
	@Test
	public void nineHundred() {
		assertEquals("900", "CM", romanNumerals.arabicToRoman(900));
	}
	
	@Test
	public void oneThousand() {
		assertEquals("1000", "M", romanNumerals.arabicToRoman(1000));
	}
	
	@Test
	public void mixArabicToRoman() {
		assertEquals("1223", "MCCXXIII", romanNumerals.arabicToRoman(1223));
		assertEquals("1999", "MCMXCIX", romanNumerals.arabicToRoman(1999));
		assertEquals("2781", "MMDCCLXXXI", romanNumerals.arabicToRoman(2781));
		assertEquals("4567", "MMMMDLXVII", romanNumerals.arabicToRoman(4567));
		assertEquals("321", "CCCXXI", romanNumerals.arabicToRoman(321));
		assertEquals("3266", "MMMCCLXVI", romanNumerals.arabicToRoman(3266));
		assertEquals("1211", "MCCXI", romanNumerals.arabicToRoman(1211));
		assertEquals("4321", "MMMMCCCXXI", romanNumerals.arabicToRoman(4321));
		assertEquals("4789", "MMMMDCCLXXXIX", romanNumerals.arabicToRoman(4789));
		assertEquals("1207", "MCCVII", romanNumerals.arabicToRoman(1207));	
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void illegalArgumentTest(){
		romanNumerals.arabicToRoman(0);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void illegalArgumentTest2(){
		romanNumerals.arabicToRoman(-10);
	}
	
	@Test
	public void mixRomanToArabic() {
		assertEquals("I", 1, romanNumerals.romanToArabic("I"));
		assertEquals("II", 2, romanNumerals.romanToArabic("II"));
		assertEquals("III", 3, romanNumerals.romanToArabic("III"));
		assertEquals("IV", 4, romanNumerals.romanToArabic("IV"));
		assertEquals("V", 5, romanNumerals.romanToArabic("V"));
		assertEquals("VI", 6, romanNumerals.romanToArabic("VI"));
		assertEquals("VII", 7, romanNumerals.romanToArabic("VII"));
		assertEquals("VIII", 8, romanNumerals.romanToArabic("VIII"));
		assertEquals("IX", 9, romanNumerals.romanToArabic("IX"));
		assertEquals("X", 10, romanNumerals.romanToArabic("X"));
		assertEquals("XI", 11, romanNumerals.romanToArabic("XI"));
		assertEquals("XII", 12, romanNumerals.romanToArabic("XII"));
		assertEquals("XIII", 13, romanNumerals.romanToArabic("XIII"));
		assertEquals("XIV", 14, romanNumerals.romanToArabic("XIV"));
		assertEquals("XV", 15, romanNumerals.romanToArabic("XV"));
		
		assertEquals("XL", 40, romanNumerals.romanToArabic("XL"));
		assertEquals("L", 50, romanNumerals.romanToArabic("L"));
		assertEquals("XC", 90, romanNumerals.romanToArabic("XC"));
		assertEquals("C", 100, romanNumerals.romanToArabic("C"));
		assertEquals("CD", 400, romanNumerals.romanToArabic("CD"));
		assertEquals("D", 500, romanNumerals.romanToArabic("D"));
		assertEquals("CM", 900, romanNumerals.romanToArabic("CM"));
		assertEquals("M", 1000, romanNumerals.romanToArabic("M"));
		
		assertEquals("MCCXXIII", 1223, romanNumerals.romanToArabic("MCCXXIII"));
		assertEquals("MCMXCIX",1999, romanNumerals.romanToArabic("MCMXCIX"));
		assertEquals("MMDCCLXXXI",2781, romanNumerals.romanToArabic("MMDCCLXXXI"));
		assertEquals("MMMMDLXVII",4567, romanNumerals.romanToArabic("MMMMDLXVII"));
		assertEquals("CCCXXI", romanNumerals.romanToArabic("CCCXXI"));
		assertEquals("MMMCCLXVI",321, romanNumerals.romanToArabic("MMMCCLXVI"));
		assertEquals("MCCXI",1211, romanNumerals.romanToArabic("MCCXI"));
		assertEquals("MMMMCCCXXI",4321, romanNumerals.romanToArabic("MMMMCCCXXI"));
		assertEquals("MMMMDCCLXXXIX",4789, romanNumerals.romanToArabic("MMMMDCCLXXXIX"));
		assertEquals("MCCVII",1207, romanNumerals.romanToArabic("MCCVII"));	
	}

}
